<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class user extends Model
{
    protected $fillable =['fname_user','lname_user','email_user','password_user'];
    protected $table = "user";

    public function blog(){
        return $this->hasMany('App\blog','ID_user','ID_user');
    }
}
