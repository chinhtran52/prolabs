<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\blog_detail;
use App\blog;
use App\user;
use Session;
//use Illuminate\Support\Facades\Session;

class PageController extends Controller
{
    //
    public function getBlog(){
        return view('blog');
    }

    public function getLabPage(){
        return view('lab-blog');
    }

    public function getTest(){
        return view('test');
    }

    public function getSingleBlog(){
        $data_blog_detail = blog_detail::all();
        return view('single-blog',compact('data_blog_detail'));
    }

    public function getInfoBlogs(){
        $data = blog_detail::all();
        return view('blog',compact('data'));
    }

    /*public function postRegister(Request $req){
        $user = new user();
        $user->fname_user = $req->input('fname');
        $user->lname_user = $req->lname;
        $user->email_user = $req->email;
        $user->password_user = $req->password;
        $user->save();
        DB::insert('insert into user (fname_user, lname_user, email_user, password_user) values (?, ?)', [13, 'FirebirD']);
    }*/
}
