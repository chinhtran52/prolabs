<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class blog extends Model
{
    protected $table = "blog";

    public function blog_detail(){
        return $this->hasMany('App\blog_detail','ID_blog','ID_blog');
    }

    public function user(){
        return $this->belongsTo('App\user','ID_user','ID_user');
    }
}
