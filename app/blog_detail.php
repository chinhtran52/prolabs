<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class blog_detail extends Model
{
    protected $table = "blog_detail";

    public function blog(){
        return $this->belongsTo('App\blog','ID_blog','ID_blog');
    }
}
