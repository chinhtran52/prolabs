<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
Route::get('test', 'PageController@getTest');

Route::get('singleblog','PageController@getSingleBlog');

Route::get('/', function () {
    return view('welcome');
});

Route::get('welcome', function () {
    return view('welcome');
});

Route::get('/index', function () {
    return view('welcome');
});

Route::get('/index', function () {
    return view('welcome');
});

Route::get('/chinh', function () {
    return view('chinh');
});

Route::get('blog','PageController@getInfoBlogs');

Route::get('lab','PageController@getLabPage');

Route::get('/contact', function () {
    return view('contact');
});

Route::get('/about', function () {
    return view('about');
});

Route::get('/listings', function () {
    return view('listings');
});

Route::get('/listings-single', function () {
    return view('listings-single');
});

Route::get('/login-signup', function () {
    return view('login-signup');
});

Route::resource('member', 'MemberController');
