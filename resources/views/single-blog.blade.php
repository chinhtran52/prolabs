@extends('master')
@section('main')
<div class="site-blocks-cover inner-page-cover overlay" style="padding-bottom:800pt;background-image: url(images/hero_1.jpg);" data-aos="fade" data-stellar-background-ratio="0.5">
    <div class="container" style="padding-top:30px">
      @foreach($data_blog_detail as $link)
        <?php 
        $id = $_GET['id'];
        ?>
        @if($link->id_blog == $id)     
        <div class="row align-items-center justify-content-center text-center">
          <div class="col-md-10">
            <h1>{{$link->name_blog}}</h1>
            <iframe width="560" height="315" src="https://www.youtube.com/embed/{{$link->link_youtube}}" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
          </div>
          <div class="col-sm-8" style="border-radius:30px; padding:20px 20px 20px 20px;background-color:white; margin-top:50px; margin-left:auto; margin-right:auto;">
        <h2 class="text-primary mb-3">Tên tác giả</h2>
        <p class="text-dark">
          {{$link->decription_blog}}
        </p>
        <h3 class="text-primary mb-3">Bình luận</h3>
        <!--bình luận fb-->
        <div id="fb-root"></div>
        <script async defer crossorigin="anonymous" src="https://connect.facebook.net/vi_VN/sdk.js#xfbml=1&version=v3.3&appId=365796214050297&autoLogAppEvents=1"></script>
        <div class="fb-comment-embed" data-href="https://www.facebook.com/zuck/posts/10102577175875681?comment_id=1193531464007751&amp;reply_comment_id=654912701278942" data-width="560" data-include-parent="true"></div>
        </div>
        </div> 
        @endif
      @endforeach
    </div>
</div>
@stop