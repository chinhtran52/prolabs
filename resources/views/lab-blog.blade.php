@extends('master')
@section('main')
<div class="site-blocks-cover inner-page-cover overlay" style="padding-bottom:700pt;background-image: url(images/hero_1.jpg);" data-aos="fade" data-stellar-background-ratio="0.5">
    <div class="container" style="padding-top:30px">
      <div class="row align-items-center justify-content-center text-center">
        <div class="col-md-10">
          <h1>Chemical Labs</h1>
        </div>
        <div class="col-sm-8" style="height:800px; border-radius:30px; padding:50px 20px 20px 20px;background-color:white; margin-left:auto; margin-right:auto;">
            <h2 class="text-primary mb-3">Hiện tượng phản ứng tạo nên Natri Hydroxit</h2>
            <div class="row align-items-stretch" style="margin-top:50px;margin-left:auto; margin-right:auto;">
              <div class="col-sm-4">
              <label for="sel1">Chất tham gia 1:</label>
              <select class="form-control" id="sel1" onchange="validateSelectBox(this,1)">
                <option>Water</option>
                <option>Hydrochloric Acid</option>
                <option>Salpetre Acid</option>
                <option>Sodium Hydroxide</option>
              </select>

              <div class="element-card" style="margin-left:auto; margin-right:auto;">
                <div class="front-facing">
                  <h1 id="Formula1" class="abr">H2O</h1>
                  <p id="Name1" class="title">Water</p>
                  <span id="Form1" class="atomic-number">Compound</span>
                  <span id="Molecular1" class="atomic-mass">18</span>
                </div>
                <div class="back-facing">
                  <h6>Liquid</h6>
                  <h6>Nontoxique</h6>
                  <h6>Color: Non</h6>
                  <p><a class="btn" href="https://en.wikipedia.org/wiki/Water" target="_blank">More info</a></p>
                </div>
              </div>

              </div>
              <div class="col-sm-4">
                <img style="height:20px; width:20px;" src="http://www.clker.com/cliparts/x/a/M/2/L/m/dark-purple-plus-hi.png"/>
              </div>
              <div class="col-sm-4">
                <label for="sel1">Chất tham gia 2:</label>
                <select class="form-control" id="sel1" onchange="validateSelectBox(this,2)">
                  <option>Sodium</option>
                  <option>Chlorine</option>
                  <option>Sulfur</option>
                  <option>Calcium</option>
                </select>

                <div class="element-card" style="margin-left:auto; margin-right:auto;">
                  <div class="front-facing">
                    <h1 id="Formula2"class="abr">Na</h1>
                    <p id="Name2" class="title">Natri</p>
                    <span id="Form2" class="atomic-number">9</span>
                    <span id="Molecular2" class="atomic-mass">21</span>
                  </div>
                  <div class="back-facing">
                      <h6>Soft Solid</h6>
                      <h6>Toxique</h6>
                      <h6>Color: White</h6>
                      <p><a class="btn" href="https://en.wikipedia.org/wiki/sodium" target="_blank">More info</a></p>
                    </div>
                </div>

              </div>
            </div>
            <div class="col-md-10" style="margin-top:60px;margin-left:auto; margin-right:auto;">

              <div class="ico animated">

                <div class="circle circle-top"></div>
                <div class="circle circle-main"></div>
                <div class="circle circle-bottom"></div>

                <svg onClick="changeValue()" class="svg" version="1.1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px" viewBox="0 0 612 612" style="enable-background:new 0 0 612 612;" xml:space="preserve" >
                <defs>
                  <clipPath id="cut-off-arrow">
                    <circle cx="306" cy="306" r="287"/>
                  </clipPath>

                  <filter id="goo">
                    <feGaussianBlur in="SourceGraphic" stdDeviation="10" result="blur" />
                    <feColorMatrix in="blur" mode="matrix" values="1 0 0 0 0  0 1 0 0 0  0 0 1 0 0  0 0 0 18 -7" result="goo" />
                    <feBlend in="SourceGraphic" in2="goo" />
                  </filter>

                </defs>
                  <path  class="st-arrow" d="M317.5,487.6c0.3-0.3,0.4-0.7,0.7-1.1l112.6-112.6c6.3-6.3,6.3-16.5,0-22.7c-6.3-6.3-16.5-6.3-22.7,0
                        l-86,86V136.1c0-8.9-7.3-16.2-16.2-16.2c-8.9,0-16.2,7.3-16.2,16.2v301.1l-86-86c-6.3-6.3-16.5-6.3-22.7,0
                        c-6.3,6.3-6.3,16.5,0,22.7l112.7,112.7c0.3,0.3,0.4,0.7,0.7,1c0.5,0.5,1.2,0.5,1.7,0.9c1.7,1.4,3.6,2.3,5.6,2.9
                        c0.8,0.2,1.5,0.4,2.3,0.4C308.8,492.6,313.8,491.3,317.5,487.6z"/>
                </svg>
              </div>

            </div>
            <!---->
            <div id="container">
              <div id="beaker">
                <div id="liquid">
                  <div class="bubble"></div>
                  <div class="bubble"></div>
                  <div class="bubble"></div>
                  <div class="bubble"></div>
                  <div class="bubble"></div>
                  <div class="bubble"></div>
                  <div class="bubble"></div>
                  <img id="kettua" style="display:none;margin-top:80px;height:27px;width:50px;"src="images/kettua.png">
                </div>
                <div id="liquid_2" style="display:none;"></div>
              </div>
            </div>
            <h2 id="pthh" class="text-primary mb-3"></h2>

            <script src='http://cdnjs.cloudflare.com/ajax/libs/jquery/2.1.3/jquery.min.js'></script>
        </div>
      </div>
    </div>
</div>
<script src='https://cdnjs.cloudflare.com/ajax/libs/jquery/3.1.1/jquery.min.js'></script>
<script src="js/element.js"></script>
<script src="js/labs.js"></script>
@stop
